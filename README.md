# Lost In Translation

This is a sign language translator, it's a Single Page Application using the React framework. 


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.



## Install


```
git clone "https://gitlab.com/rebsan00003/lost-in-translation.git"
cd lost-in-translation
npm install
```

## Usage
`npm start`

This will open a new webpage in your browser at localhost:3000

You need to create an .env file containing an API key and API url. Do this by first adding a new file to the project root and name it .env
Here you create environment variables:

```
REACT_APP_API_KEY=<your key>
REACT_APP_API_URL=<your url>
```

The key can be found under config vars in the api application settings. Open the app and copy the api url and paste it to equal the url variable.
Restart the development server.

## Contributors
Rebecka Ocampo Sandgren (@rebsan00003) and Negin Bakhtiarirad (@neginb)

## Contributing
No contributions allowed.

