const apiKey = process.env.REACT_APP_API_KEY

//use this function to add or change content in the API
export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }

}
