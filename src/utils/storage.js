//check if key does not exist or is not type of string and throw an error
const validateKey = key => {
    if (!key || typeof key !== 'string') {
        throw new Error('Invalid storage key provided')
    }
}

//save username as key and the given information as value to localStorage
export const storageSave = (key, value) => {

    validateKey(key)

    if (!value) {
        throw new Error('storageSave: No value provided for' + key)
    }
    localStorage.setItem(key, JSON.stringify(value))
}

//Read the LocalStorage based on the key
export const storageRead = key => {

    validateKey(key)

    const data = localStorage.getItem(key)
    if (data) {
        return JSON.parse(data)
    }
    return null
}

//Delete localStorage based on key
export const storageDelete = key => {

    validateKey(key)

    localStorage.removeItem(key)
}