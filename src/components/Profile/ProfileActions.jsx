import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { translationClearHistory } from '../../api/translation';
import { STORAGE_KEY_USER } from '../../const/storageKeys';
import { useUser } from '../../context/UserContext';
import { storageDelete, storageSave } from '../../utils/storage';

const ProfileActions = () => {

    const {user, setUser} = useUser()

    //loging out the user 
    const handleLogoutClick = () => {
        if(window.confirm('Are you sure?')){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    //handle the "clear history" button and clear the history list
    const handleClearHistoryClick = async () =>{
        if(!window.confirm('Are you sure?\nThis can not be undone!')){
            return
        }

        const [clearError] = await translationClearHistory(user.id)
        if(clearError !== null){
            return
        }

        const updatedUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
         
    }

    //the buttons in the profile page
    return (
        <>
            <Link className='BackButton' to="/translation">Back to translate page</Link>
            <a className='LogOutButton' onClick={ handleLogoutClick }>Log out</a>
            <button id="clearButton" onClick={ handleClearHistoryClick}>Clear History</button>
        </>
    )
}
export default ProfileActions