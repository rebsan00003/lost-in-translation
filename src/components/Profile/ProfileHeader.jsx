import React, { Component }  from 'react';

//the layout of the profile pages' header
//render logged in username
const ProfileHeader = ({username}) =>{
    return(
        <header>
            <h4>Hello, welcome back {username}</h4>
        </header>
    )
}
export default ProfileHeader