import React, { Component } from 'react';
import ProfileTranslateHistoryItem from './ProfileTranslateHistoryItem';


//get the last ten translations and create a list with the translation items
//if there is no list item, then inform the user that the list is empty
const TranslateBox = ({ translations }) => {

    const tenLastTranslations = translations.slice(-10)
    
    const translationList = tenLastTranslations.map(
        (translation, index) => <ProfileTranslateHistoryItem key={index + '-' + translation} translation={translation} />)

        

    return (

        <div className='TranslateBox'>
            <h3 id="translateTitle">Last 10 translations:</h3>

            {translationList.length === 0 && <p>You have no translations yet.</p>}

                <ul>
                    {translationList}
                </ul>
        </div>
    )

}
export default TranslateBox