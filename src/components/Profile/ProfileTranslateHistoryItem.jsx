import React, { Component }  from 'react';

//return the list item with the translation word
const ProfileTranslateHistoryItem = ({ translation }) => {
    return <li>{ translation }</li>
}
export default ProfileTranslateHistoryItem