import React, { Component, useState, useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage';
import { useNavigate } from "react-router-dom"
import { useUser } from '../../context/UserContext';
import { STORAGE_KEY_USER } from '../../const/storageKeys';

//minimum length of username is 3
const usernameConfig = {
    required: true,
    minLength: 3
}

//
const LoginForm = () => {
    //Hooks
    const { register, handleSubmit, formState: { errors }} = useForm()
    const { user, setUser} = useUser()
    const navigate = useNavigate()

    //local State
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError] = useState(null)

    //Side Effects
    //if the user is not null, navigate to translation page
    useEffect(() => {
       if(user !== null){
        navigate('translation')
       }
    }, [ user, navigate])

    //Event Handlers
    //handle login buttons' functionality
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [ error, userResponse ] = await loginUser(username)
        if (error !== null){
            setApiError(error)
        }
        if(userResponse !== null){
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)

    };
    
    //Render Error message functions
    const errorMessage = (() => {
        if(!errors.username){
            return null
        }
        if(errors.username.type === 'required'){
            return <span>Username is required</span>
        }
        if(errors.username.type === 'minLength'){
            return <span>Username is too short</span>
        }
        
    })();

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset className='InputBox'>
                    <input className='InputField'
                        type="text"
                        placeholder="What's your name?"
                        {...register("username", usernameConfig)}
                    />
                    <button type="submit" className='SubmitButton' disabled={ loading }>✔</button><br></br>
                    {errorMessage}
                </fieldset>

                { loading && <p>Logging in...</p> }
                { apiError && <p> {apiError} </p>}
            </form>
        </>
    )
}
export default LoginForm