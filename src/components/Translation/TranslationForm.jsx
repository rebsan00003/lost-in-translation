import React, { Component, useState } from "react";
import { useForm } from "react-hook-form";
import { NavLink } from "react-router-dom";
import { translationAdd } from "../../api/translation";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageSave } from "../../utils/storage";


const TranslationForm = () => {

    const { register, handleSubmit} = useForm()
    const [ letters, setLetter ] = useState([])
    const {user, setUser} = useUser()

    //make an array with the letters of the translation word and add to users' translationList
    const onSubmit = async (data) => {
      const word = data.words
        setLetter(word.split(""))
        if (!word){
          alert('Nothing to translate!')
        }
        const [ error, updatedUser] = await translationAdd(user, word)
        if(error !== null){
            return
        }

        //Keep UI state and Server state in sync
        storageSave(STORAGE_KEY_USER, updatedUser)
        //Update context state
        setUser(updatedUser)
    }
    

  return (
    <>
      <NavLink className="ProfileButton" to="/profile">
        Profile
      </NavLink>
      <form onSubmit={ handleSubmit(onSubmit)}>
        <input className="TranslateField" type="text" placeholder="Translate ..." {...register('words')}/>
        <button type="submit" id="translateButton" className="SubmitButton">✔</button>
      </form>

      {/* iterate through all the letters of the word and match with the images' name and render */}
      <div>
        <div className="TranslateBox">
          
            { letters.map( (item, index) => { return(
                <img className="Signs" src={`img/${ item }.png`} alt={ item } key={index + "-" + item}></img>
            )}) }
          
        </div>
      </div>
    </>
  );
};
export default TranslationForm;
