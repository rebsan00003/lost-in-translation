import React, { Component } from "react";
import TranslationForm from "../components/Translation/TranslationForm";
import withAuth from "../hoc/withAuth";


//the layout of the Translation page 
const Translation = () => {

  return (
    <>
      <div className="HalfPageTranslate"></div>
      <div className="WhitePage"></div>
      <TranslationForm />
    </>
  );
};

export default withAuth(Translation);