import LoginForm from "../components/Login/LoginForm";
import React, { Component } from "react";
import picture from '../Logo.png'

//the layout of the Login page 
const Login = () => {
  return (
    <>
      <div className="HalfPage">
        <img src={picture} className="AppLogo" alt="logo"></img>
        <div className="halfpageText">
          <h1>Lost in translation</h1>
          <h2>Get started</h2>
        </div>
      </div>
      
      <div className="WhitePage">

      </div>
      <LoginForm />
    </>
  );
};

export default Login;
