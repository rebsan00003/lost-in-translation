import React, { Component }  from 'react';
import ProfileActions from '../components/Profile/ProfileActions';
import ProfileHeader from '../components/Profile/ProfileHeader';
import TranslateBox from '../components/Profile/TranslateBox';
import { useUser } from '../context/UserContext';
import withAuth from '../hoc/withAuth';


//the layout of the Profile page 
const Profile = () => {

    const {user} = useUser()

    return (
        <>
            <ProfileHeader username={user.username}/>
            <TranslateBox translations={user.translations}/>
            <ProfileActions />
        </>
    )
}

export default withAuth(Profile) 