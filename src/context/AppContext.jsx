import UserProvider from "./UserContext"
import React, { Component} from 'react';

//responsible for merging all contexts
const AppContext = ({children}) => {
    return (
        <UserProvider>
            {children}
        </UserProvider>
    )
}
export default AppContext