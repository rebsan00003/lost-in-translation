
import React from 'react'
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Login from './views/Login';
import Profile from './views/Profile';
import Translation from './views/Translation';
import Header from './components/Header/Header';

function App() {
  
  return (
    <BrowserRouter>
      <div className="App">
        <Header></Header>
        <Routes>
          <Route path="/" element={<Login />}/>
          <Route path="/profile" element={<Profile />}/>
          <Route path="/translation" element={<Translation />}/>
        </Routes>
      </div>
    </BrowserRouter>
    
  );
}


export default App;
